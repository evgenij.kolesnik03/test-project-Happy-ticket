<?php

namespace app\models\forms;

use yii\base\Model;


class LuckyTicketForm extends Model
{
    public $from;
    public $to;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'number', 'min'=> 1, 'max'=> 999999 ],
            [['from'], function ($model, $param) {
                if((int)$this->from > (int)$this->to){
                    $this->addError('from', "From cannot be less than To");
                }
            }],
            [['to'], function ($model, $param) {
                if((int)$this->from > (int)$this->to){
                    $this->addError('to', "To cannot be less than From");
                }
            }]
        ];
    }
}
