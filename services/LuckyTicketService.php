<?php

namespace app\services;

use app\models\forms\LuckyTicketForm;

class LuckyTicketService
{
    /**
     * @param LuckyTicketForm $form
     * @return int
     */
    public function calculation(LuckyTicketForm $form)
    {
        $counter = 0;
        for ($i = $form->from; $i <= $form->to; $i++) {
            $counter += $this->hasLuckyTicket($i);
        }

        return $counter;
    }

    /**
     * @param int $number
     * @return int
     */
    private function hasLuckyTicket(int $number)
    {
        $number = (string)$number;

        $resultCombinationPartOne = $this->combinationNumber(substr($this->filterNumber($number), 0, 3));
        $resultCombinationPartTwo = $this->combinationNumber(substr($this->filterNumber($number), 3, 3));

        if ($resultCombinationPartOne === $resultCombinationPartTwo) {
            return 1;
        }

        return 0;
    }

    private function filterNumber(string $number)
    {
        if (strlen($number) == 6) {
            return $number;
        }

        $lengthDifference = 6 - strlen($number);
        for ($i = 0; $i < $lengthDifference; $i++) {
            $number = '0' . $number;
        }

        return $number;
    }

    private function combinationNumber($numbPart)
    {
        $result = 0;
        $arr = str_split($numbPart);
        foreach ($arr as $value) {
            $result += intval($value);
        }

        if (strlen($result) > 1) {
            $result = $this->combinationNumber($result);
        }

        return $result;
    }
}