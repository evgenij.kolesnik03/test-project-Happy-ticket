<?php

/** @var yii\web\View $this */

$this->title = 'Test Kolesnik Evgenij';
?>
<div class="jumbotron text-center bg-transparent">
    <h1 class="display-4"><?=$this->title?></h1>
</div>
<?php $form = \yii\widgets\ActiveForm::begin([
    'id' => 'my-form-id',
    'action' => '/site/lucky-ticket-request',
    'enableAjaxValidation' => true,
    'validationUrl' => '/site/validate',
]); ?>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'from')->textInput()->label('N - from'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'to')->textInput()->label('N - to'); ?>
        </div>
        <div class="col-md-4">
            <p></p>
            <?= \yii\helpers\Html::submitButton('Run', ['class' => 'btn-run']); ?>
        </div>
    </div>
</div>
<div class="lucky-ticket-answer">
    <strong>Number of tikets: </strong><span>0</span>
</div>
<?php $form->end(); ?>
<?php
$js = <<< JS
$('#my-form-id').on('beforeSubmit', function () {
    $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serializeArray()
        }
    )
    .done(function(data) {
        $('.lucky-ticket-answer').find('span').html(data.result ? data.result : 0);
    })
 
return false; // отменяем отправку данных формы
})
JS;
$this->registerJs($js, $position = yii\web\View::POS_END);
?>
